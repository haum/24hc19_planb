Évaluation
==========

L’évaluation des solutions est effectuée en partie de manière continue (lors des
points collectifs ou lorsque vous évoquez vos choix techniques, vos avancées et vos
difficultés aux porteurs du sujet par exemple) et par une présentation que vous ferez
à tous (avec démonstration si possible) à la fin de l’épreuve (pensez à dormir).
Nous accorderons beaucoup d’ importance à :

- Originalité/innovation
- Pertinence des choix techniques
- Appréciation et implication collectives
- Qualité de la prise en compte de tous les objectifs
- Facilité d’utilisation et complexité des scenarii réalisables par l’utilisateur final

