Déroulement
===========

Vous avez accès à un réseau de test sur lequel des laumios et les capteurs sont installés.
Les équipes doivent s’organiser afin qu’ il n’y ait pas de monopolisation du terrain (note
: selon la nature des tests, il est même possible de partager cet espace en même temps).
Il peut être utile de mettre en place des micro-simulateurs sur votre machine pour
effectuer les tests en dehors de l’ infrastructure physique.

Les sujets proposés par le HAUM n’ont pas l’habitude d’être à visée scolaire. Ce sujet-ci
ne déroge pas à la règle et est tout à fait dans l’esprit du hackerspace : ce week-end,
nous partageons collectivement une aventure expérimentale. Ce qui nous intéresse n’est pas
votre capacité à résoudre un problème fermé mais bien l’ ingéniosité que vous mettrez pour
imaginer une solution possible à cette problématique atypique.

Parce que les 24h c’est aussi et surtout un temps de partage entre passionnés. Les équipes
du sujet HAUM Sweet OHM se verront proposer d’aller boire un verre pour se détendre et
faire connaissance au cours de la nuit.

Des séances collectives (obligatoires) avec toutes les équipes sont prévues tout au long
de l’évènement pour faire le point et discuter des difficultés et des succès. Nous serons
présents au cours de la nuit.
