Les Objectifs
=============

Dans le cadre de ce lieu partagé, ingéniosité et originalité sont de rigueur. Votre
projet doit par ailleurs être facile d’utilisation sous peine de le voir remplacer par celui
d’une autre équipe qui aura davantage séduit. Plusieurs petits défis s’agrègent pour
satisfaire au mieux les futurs utilisateurs.


La base
-------

Vous devez réaliser ces tâches dans l’ordre indiqué. Elles ont un objectif didactique
et nous permettent d’évaluer la vitesse de progression des équipes.

- Être capable de choisir la couleur des laumios
- Être capable d’animer les laumios (fournir plusieurs animations, éventuellement configurables)
- Gérer l’allumage/animation des laumios par groupes ou individuellement
- Récupérer les informations des capteurs et y réagir (callback)

Attentes spécifiques
--------------------

Vous pouvez réaliser ces tâches dans l’ordre que vous souhaitez. Vous êtes également libres, une fois tous ces points traités, de perfectionner votre système.

- Réagir à des sources extérieures (ex. Mastodon, Wikipedia, météo, manifestations...)
- Avoir une interface quelconque (ex. CLI, application mobile, interface web, application PC...) pour interagir facilement avec l’ installation
- Permettre de programmer soi-même des réactions aux évènements (note : certains utilisateurs ne connaissent pas les langages de programmation, soyez créatifs)

Les utilisateurs sont pointilleux quant à la simplicité de programmation mais aussi à la complexité des scenarii qu’ ils pourront mettre en œuvre avec votre outil


