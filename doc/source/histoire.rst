Histoire
========

Yuang-Lu aime beaucoup les Laumios [#f1]_ du Haum et en a fabriqué une collection pour décorer son hackerspace local. En bons bidouilleurs, les occupants du lieu souhaitent aller plus loin avec ce système et façonner l’ambiance en fonction des évènements du laboratoire et du monde extérieur.

Tsai-Hen s’occupera d’agrémenter le lieu de capteurs physiques tandis que votre équipe chevronnée a choisi de rivaliser d’ ingéniosité pour concevoir la partie logicielle.

Bien motivés, Yuang-Lu et Tsai-Hen ont déjà mis en réseau les laumios et quelques capteurs. De votre côté, vous avez choisi de faire tourner votre logiciel sur une de vos machines personnelles qu’ il suffira de relier à ce réseau pour le premier prototype présenté dimanche. Vous avez donc tout pour commencer les expérimentations.


.. [#f1] Lampes multicolores pouvant être animées et pilotées en réseau https://haum.org/pages/laumios.html
