Contrôler la musique: pont MQTT MPD
===================================

Pour diffuser de la musique dans le hackerspace, c'est MPD_ qui a été retenu.
Ce programme expose une interface telnet sur le port 6600 par défaut (les commandes sont
présentées dans sa documentation_).

Afin de contrôler ce programme, les hackers ont écrit un pont entre l'interface telnet de
MPD et MQTT que cette page décrit.

.. _MPD: https://www.musicpd.org/
.. _documentation: https://www.musicpd.org/doc/html/user.html

Structure
---------

Un petit `script Python`_ relié au broker MQTT écoute des commandes sur les topics
``music/control/<commande>``. La payload associée permet au besoin de passer des arguments
à la commande.

Seules les commande de base sont exposées dans MQTT.

Commandes
---------

Les commandes fonctionnelles sont les suivantes (sauf mention contraire, la payload est ignorée et le script ne renvoie rien):

``music/control/getstate``
	retourne l'état de lecture dans ``music/status`` (*play*, *pause* ou *stop*)
``music/control/getvol``
	retourne le volume courant (entre 0 et 100) dans ``music/status``
``music/control/next``
	passe au morceau suivant
``music/control/previous``
	passe au morceau précédent
``music/control/stop``
	arrête la musique
``music/control/play``
	(re)démarre la lecture
``music/control/pause``
	met la lecture en pause
``music/control/toggle``
	agit comme un bouton Play/Pause (met en pause si en lecture et en lecture si stoppé ou pausé)
``music/control/setvol``
	permet de règler le volume au moyen d'un paramètre en payload entre 0 et 100 (entier uniquement)


.. _script Python: https://github.com/haum/mqtt_mpd
