Les capteurs
============

Les capteurs dialoguent exclusivement avec le protocole MQTT. Ils publient leur
état et réagissent à quelques commandes par le biais de topics MQTT détaillés
par la suite.

Capteur Bouttons poussoir
-------------------------

Il s'agit d'un ensemble de quatre boutons poussoir associés à quatre LEDs. Un
appui sur le boutton change l'état de la LED associée et transmet l'état de ces
deux entités sur le topic concerné. L'état des LEDs peut être piloté
indépendamment également.

Le capteur publie les informations suivantes :

``capteur_bp/status`` :
   Il s'agit du statut de connexion du capteur qui peut valoir "online" ou
   "offline". Cette donnée est émise par le broker à la connexion au topic.

``capteur_bp/switch/ledX/state`` :
   Il s'agit de l'état de la led n°X (X de 1 à 4 inclus). La valeur peut être
   "ON" ou "OFF". Cette donnée est émise par le broker à la connexion au topic.

``capteur_bp/binary_sensor/bpX/state`` :
   Il s'agit de l'état du bouton poussoir n°X (X de 1 à 4 inclus). La valeur
   peut être "ON" ou "OFF". Cette donnée est émise lors du changement d'état du
   bouton.

``capteur_bp/sensor/bp_rssi/state`` :
   Il s'agit du niveau du signal wifi du capteur. Cette donnée est transmise
   lors du changement de sa valeur. Elle est publiée comme une chaîne de caractères
   de la valeur en décibels l'indicateur de réception du signal. Elle est mise à
   jour toutes les 15 secondes.

``capteur_bp/sensor/uptime_sensor/state``
   Il s'agit du temps de fonctionnement (uptime) du capteur. La valeur est
   exprimée en minutes.  Cette donnée est transmise lors du changement de sa
   valeur.

Le capteur reçoit les commandes suivantes :

``capteur_bp/switch/ledX/command`` :
   Il s'agit de la commande de la led n°X (X de 1 à 4 inclus). Le contenu du
   message attendu est "ON" ou "OFF".

``capteur_bp/status/advertise``
   Il s'agit d'une commande de découverte. Quel que soit le contenu du message,
   la réponse est publiée sur le topic ``capteur_bp/status`` avec la valeur
   "online" si le capteur est présent.

Télécommande infrarouge
-----------------------

Il s'agit d'un récepteur de télécommande infrarouge. La télécommande dispose de
21 touches dont les états sont publiés sur les topics :

``remote/NOM_DE_LA_TOUCHE/state`` avec NOM_DE_LA_TOUCHE la touche correspondante
(voir la liste qui suit). Chaque appui publie deux messages, le premier avec un
payload "ON" à la pression et le second avec le message "OFF" au relâchement.

les touches disponible sur la télécommande sont les suivantes:

+-------+------+------+
| power | mode | mute |
+-------+------+------+
| playp | prev | next |
+-------+------+------+
|   eq  | minus| plus |
+-------+------+------+
|   0   | chg  | u_sd |
+-------+------+------+
|   1   |  2   |   3  |
+-------+------+------+
|   4   |  5   |   6  |
+-------+------+------+
|   7   |  8   |   9  |
+-------+------+------+


Capteur détecteur de présence
-----------------------------

Ce capteur indique la présence d'une personne dans son champ de détection.

Le capteur publie les informations suivantes :

``presence/state`` :
   Il s'agit de l'état de la détection par le capteur. Les valeurs peuvent être
   "OFF" lorsqu'il n'y a pas de détection ou "ON" pour une présence devant le
   capteur. Il publie un nouvel état a chaque changement de celui-ci.

``presence/status`` :
   Il s'agit du statut de connexion du capteur qui peut valoir "online" ou
   "offline". Cette donnée est émise par le broker à la connexion au topic.

Le capteur reçoit la commande suivante :

``presence/status/advertise`` :
   Il s'agit d'une commande de découverte. Quel que soit le contenu du message,
   la réponse est publiée sur le topic ``presence/status`` avec la valeur
   "online" si le capteur est présent.


Capteur de distance
-------------------

Il s'agit d'un capteur qui donne une indication de la distance le séparant du
plus proche objet dans son champ de détection.

Le capteur publie les informations suivantes :

``distance/value`` :
   Il s'agit de la distance de l'objet lui faisant face exprimée en mètres. La
   précision est de quelques centimètres. Elle est publiée comme une chaine de caractères
   de la valeur en mètres sous la forme X.XX . Elle est mise à jour toutes les 5 secondes
   quand un objet est dans le périmètre de détection.


``distance/status`` :
   Il s'agit du statut de connexion du capteur qui peut valoir "online" ou
   "offline". Cette donnée est émise par le broker à la connexion au topic.

Le capteur reçoit la commandes suivante :

``distance/status/advertise`` :
   Il s'agit d'une commande de découverte. Quel que soit le contenu du message,
   la réponse est publiée sur le topic ``distance/status`` avec la valeur
   "online" si le capteur est présent.

Capteur atmosphérique
---------------------

Il s'agit d'un capteur de métrologie atmosphérique : température, pression,
humidité.

Le capteur publie les informations suivantes :

``atmosphere/status`` :
   Il s'agit du statut de connexion du capteur qui peut valoir "online" ou
   "offline". Cette donnée est émise par le broker à la connexion au topic.

``atmosphere/temperature`` :
   Il s'agit de la température mesurée exprimée en degrés Celsius. La valeur est
   mise à jour toutes les 15 secondes. Elle est publiée sous forme d'chaine de
   caractères sans indication d'unité.


``atmosphere/pression`` :
   Il s'agit de la pression atmosphérique mesurée exprimée en hPa. La valeur est
   mise à jour toutes les 15 secondes. Elle est publiée sous forme d'chaine de
   caractères sans indication d'unité.

``atmosphere/humidite`` :
   Il s'agit de l'humidité relative mesurée exprimée en pourcent. Elle est publiée
   sous forme d'une chaine de caractères sans indication d'unité. La valeur
   est mise à jour toutes les 15 secondes.


``atmosphere/humidite_absolue`` :
   Il s'agit de l'humidité absolue mesurée exprimée en g/m^3. La valeur est mise
   à jour toutes les 15 secondes. Elle est publiée sous forme d'chaine de
   caractères sans indication d'unité.

Le capteur reçoit la commandes suivante :

``atmosphere/status/advertise`` :
   Il s'agit d'une commande de découverte. Quel que soit le contenu du message,
   la réponse est publiée sur le topic ``atmosphere/status`` avec la valeur
   "online" si le capteur est présent.
