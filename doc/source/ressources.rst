Ressources
==========

Réseau
------

ESSID
  HAUMSweetOHM
Pass
  Bienvenue aux 24 Heures du code.

Broker MQTT
-----------

Hostname
  mpd.lan
Port
  1883
User/Pass
  (aucun)

Websockets

Port
  1884
User/Pass
  (aucun)

Serveur MPD
-----------

Hostname
  mpd.lan
Port
  6600
User/Pass
  (aucun)

Laumios
-------

Code
  `github.com/laumio`_
Documentation
  `laumiomqtt.rtfd.io`_

MQTT MPD
--------

Code
  `github.com/mqtt_mpd`_
Documentation
  `mqtt-mpd`_



.. _github.com/laumio: https://github.com/haum/laumio/tree/MQTT_
.. _laumiomqtt.rtfd.io: https://laumiomqtt.readthedocs.io/fr/latest/
.. _github.com/mqtt_mpd: https://github.com/haum/mqtt_mpd
.. _mqtt-mpd: /mqtt-mpd.html
